package bet.pixel.wallet;

import bet.pixel.wallet.controllers.WalletController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@SpringBootTest
class WalletApplicationTests {

	@Autowired
	private WalletController walletController;

	@Test
	void contextLoads() {
		assertThat(walletController).isNotNull();
	}

}
