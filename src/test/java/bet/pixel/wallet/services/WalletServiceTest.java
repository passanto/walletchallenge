package bet.pixel.wallet.services;

import bet.pixel.wallet.Constants;
import bet.pixel.wallet.exception.NotFoundException;
import bet.pixel.wallet.exception.TransactionRequestException;
import bet.pixel.wallet.models.Account;
import bet.pixel.wallet.models.Transaction;
import bet.pixel.wallet.repositories.AccountRepository;
import bet.pixel.wallet.repositories.TransactionRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import static java.lang.String.format;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
class WalletServiceTest {

    @Mock
    private AccountRepository accountRepository;

    @Mock
    private TransactionRepository transactionRepository;

    @InjectMocks
    private WalletService walletService;


    @Test
    void getAccount() {
        long playerId = 1L;
        Account accountModel = new Account(playerId);

        given(accountRepository.findById(playerId)).willReturn(Optional.of(accountModel));

        final Account account = walletService.getAccount(playerId);

        assertThat(account).isEqualTo(accountModel);
    }

    @Test
    void addAccount() {
        long playerId = 1L;
        long accountId = 2L;
        Account accountModel = new Account(playerId);
        Account accountModelWithId = new Account(playerId, accountId);

        given(accountRepository.save(accountModel)).willReturn(accountModelWithId);

        final Account account = walletService.addAccount(playerId);

        assertThat(account).isEqualTo(accountModelWithId);
    }

    @Test
    void getBalanceByPlayerId() throws NotFoundException {
        long playerId = 1L;
        long accountId = 2L;
        BigDecimal balance = new BigDecimal("19.99");
        Account accountModelWithId = new Account(playerId, accountId, balance);

        given(accountRepository.findAccountByPlayerId(playerId)).willReturn(Optional.of(accountModelWithId));

        final BigDecimal balanceModel = walletService.getBalanceByPlayerId(playerId);

        assertThat(balance).isEqualTo(balanceModel);
    }

    @Test
    void getTransactionsByPlayerId() throws NotFoundException {
        long txId = 99L;
        long playerId = 1L;
        long accountId = 2L;
        String externalTransactionId = "externalId";
        BigDecimal balance = new BigDecimal("19.99");
        Account accountModelWithId = new Account(playerId, accountId, balance);
        Transaction tx1 = new Transaction(accountModelWithId, balance, balance, externalTransactionId);
        accountModelWithId.getTransactions().add(tx1);

        given(accountRepository.findAccountByPlayerId(playerId)).willReturn(Optional.of(accountModelWithId));

        final List<Transaction> transactions = walletService.getTransactionsByPlayerId(playerId);

        assertThat(transactions).isEqualTo(accountModelWithId.getTransactions());
    }

    @Test
    void debitForPlayerAllIn() throws TransactionRequestException, NotFoundException {
        long playerId = 1L;
        long accountId = 2L;
        String externalTransactionId = "externalId";
        BigDecimal balanceAndAmount = new BigDecimal("19.99");
        Account accountModelWithId = new Account(playerId, accountId, balanceAndAmount);

        given(accountRepository.save(accountModelWithId)).willReturn(accountModelWithId);
        given(accountRepository.findAccountByPlayerId(playerId)).willReturn(Optional.of(accountModelWithId));

        final BigDecimal amount = walletService.debitForPlayer(playerId, "123", balanceAndAmount, externalTransactionId);

        assertThat(amount).isEqualTo(new BigDecimal("0.00"));
    }

    @Test
    void debitForPlayerWillFailWithNotEnoughBalance() {
        long playerId = 1L;
        long accountId = 2L;
        String externalTransactionId = "externalId";
        BigDecimal balance = new BigDecimal("19.99");
        Account accountModelWithId = new Account(playerId, accountId, balance);

        given(accountRepository.save(accountModelWithId)).willReturn(accountModelWithId);
        given(accountRepository.findAccountByPlayerId(playerId)).willReturn(Optional.of(accountModelWithId));

        Exception exception = assertThrows(TransactionRequestException.class, () -> walletService.debitForPlayer(playerId, "123", balance.add(BigDecimal.ONE), externalTransactionId));
        assertEquals(exception.getMessage(), format(Constants.NOT_ENOUGH_FUNDS, playerId));
    }

    @Test
    void creditForPlayer() throws TransactionRequestException, NotFoundException {
        long playerId = 1L;
        long accountId = 2L;
        String externalId = "externalId";
        BigDecimal balance = new BigDecimal("19.99");
        Account accountModelWithId = new Account(playerId, accountId, balance);

        given(accountRepository.save(accountModelWithId)).willReturn(accountModelWithId);
        given(accountRepository.findAccountByPlayerId(playerId)).willReturn(Optional.of(accountModelWithId));

        final BigDecimal amount = walletService.creditForPlayer(playerId, "123", balance, externalId);

        assertThat(amount).isEqualTo(new BigDecimal("39.98"));
    }

}