package bet.pixel.wallet.controllers;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static bet.pixel.wallet.Constants.INVALID_TX_ID;
import static bet.pixel.wallet.Constants.NOT_ENOUGH_FUNDS;
import static java.lang.String.format;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
@TestMethodOrder(OrderAnnotation.class)
class WalletControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    @Order(1)
    void getInitialAccount() throws Exception {
        String accountResponse = "{\"accountId\":1,\"playerId\":1,\"transactionId\":null,\"balance\":0,\"transactions\":[],\"version\":0}";
        long playerId = 1L;
        String url = "/wallet/account?player-id=" + playerId;

        this.mockMvc.perform(get(url)).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(accountResponse));
    }

    @Test
    @Order(2)
    void addInitialAccountShouldFail() throws Exception {
        long playerId = 1L;
        String url = "/wallet/account?player-id=" + playerId;

        this.mockMvc.perform(post(url)).andDo(print())
                .andExpect(status().is4xxClientError());
    }

    @Test
    @Order(3)
    void addNewAccountShouldSucceed() throws Exception {
        String accountResponse = "{\"accountId\":2,\"playerId\":9,\"transactionId\":null,\"balance\":0,\"transactions\":[],\"version\":0}";
        long playerId = 9L;
        String url = "/wallet/account?player-id=" + playerId;

        this.mockMvc.perform(post(url)).andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().string(accountResponse));
    }

    @Test
    @Order(4)
    void getInitialZeroBalanceByPlayerId() throws Exception {
        long playerId = 1L;
        String url = "/wallet/balance?player-id=" + playerId;

        this.mockMvc.perform(get(url)).andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().string("0"));
    }

    @Test
    @Order(5)
    void getTransactionsByPlayerId() throws Exception {
        long playerId = 1L;
        String url = "/wallet/transactions?player-id=" + playerId;

        this.mockMvc.perform(get(url)).andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().string("[]"));
    }

    @Test
    @Order(6)
    void debitNotEnoughForPlayer() throws Exception {
        long playerId = 1L;
        String txId = "1";
        String externalTransactionId = "externalTransactionId";
        String url = "/wallet/debit?player-id=" + playerId + "&transaction-id=" + txId + "&external-transaction-id=" + externalTransactionId + "&amount=0.1";

        this.mockMvc.perform(post(url)).andDo(print())
                .andExpect(status().is4xxClientError())
                .andExpect(content().string(format(NOT_ENOUGH_FUNDS, playerId)));
    }

    @Test
    @Order(7)
    void creditForPlayer() throws Exception {
        long playerId = 1L;
        String txId = "1";
        String externalTransactionId = "externalTransactionId";
        String url = "/wallet/credit?player-id=" + playerId + "&transaction-id=" + txId + "&external-transaction-id=" + externalTransactionId + "&amount=1";

        this.mockMvc.perform(post(url)).andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().string("1"));
    }

    @Test
    @Order(8)
    void idempotentDebit() throws Exception {
        long playerId = 1L;
        String txId = "1";
        String externalTransactionId = "externalTransactionId";
        String creditUrl = "/wallet/credit?player-id=" + playerId + "&transaction-id=" + txId + "&external-transaction-id=" + externalTransactionId + "&amount=1";
        String debitUrl = "/wallet/debit?player-id=" + playerId + "&transaction-id=" + txId + "&external-transaction-id=" + externalTransactionId + "&amount=0.1";


        // credit 1
        this.mockMvc.perform(post(creditUrl)).andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().string("1"));

        // debit 0.1
        this.mockMvc.perform(post(debitUrl)).andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().string("0.9"));

        // debit 0.1
        this.mockMvc.perform(post(debitUrl)).andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().string("0.9"));
    }

    @Test
    @Order(9)
    void idempotentCredit() throws Exception {
        long playerId = 1L;
        String txId = "1";
        String externalTransactionId = "externalTransactionId";
        String url = "/wallet/credit?player-id=" + playerId + "&transaction-id=" + txId + "&external-transaction-id=" + externalTransactionId + "&amount=1";

        this.mockMvc.perform(post(url)).andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().string("1"));

        this.mockMvc.perform(post(url)).andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().string("1"));
    }

    @Test
    @Order(10)
    void debitWithWrongTxIdForPlayer() throws Exception {
        long playerId = 1L;
        String txId = "1";
        String externalTransactionId = "externalTransactionId";
        String url = "/wallet/credit?player-id=" + playerId + "&transaction-id=" + txId + "&external-transaction-id=" + externalTransactionId + "&amount=1";

        this.mockMvc.perform(post(url)).andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().string("1"));

        txId = "2";
        externalTransactionId = "externalTransactionId2";
        url = "/wallet/debit?player-id=" + playerId + "&transaction-id=" + txId + "&external-transaction-id=" + externalTransactionId + "&amount=0.1";

        this.mockMvc.perform(post(url)).andDo(print())
                .andExpect(status().is4xxClientError())
                .andExpect(content().string(INVALID_TX_ID));
    }

}