package bet.pixel.wallet.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Account {

    public Account(long playerId, long accountId) {
        this.playerId = playerId;
        this.accountId = accountId;
    }

    public Account(long playerId, long accountId, BigDecimal balance) {
        this.playerId = playerId;
        this.accountId = accountId;
        this.balance = balance;
    }

    public Account(long playerId) {
        this.playerId = playerId;
        this.balance = BigDecimal.ZERO;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "account_id")
    private long accountId;

    @Column(name = "player_id")
    @NotNull
    private long playerId;

    @Column(name = "transaction_id")
    private String transactionId;

    @NotNull
    private BigDecimal balance;

    @OneToMany(mappedBy = "account")
    private List<Transaction> transactions = new ArrayList<>();

    // OPTIMISTIC LOCK
    @Version
    private Integer version;

}
