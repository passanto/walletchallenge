package bet.pixel.wallet.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Entity
@NoArgsConstructor
@Data
public class Transaction {

    public Transaction(Account account, BigDecimal amount, BigDecimal balance, String externalTransactionId) {
        this.account = account;
        this.amount = amount;
        this.balance = balance;
        this.externalTransactionId = externalTransactionId;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private long id;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "account_id", nullable = false)
    @JsonIgnore
    @ToString.Exclude
    private Account account;

    @NotNull
    private BigDecimal amount;

    @NotNull
    private BigDecimal balance;

    @Column(name = "external_transaction_id")
    @NotNull
    private String externalTransactionId;

}
