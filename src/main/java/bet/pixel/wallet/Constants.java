package bet.pixel.wallet;

public final class Constants {

    private Constants() {
    }

    public static final String NO_ACCOUNT_FOR_PLAYER_ID_S = "No account for player id: %s";
    public static final String NOT_ENOUGH_FUNDS = "Account of player %s has not enough funds";
    public static final String INVALID_TX_ID = "Invalid tx id";
}
