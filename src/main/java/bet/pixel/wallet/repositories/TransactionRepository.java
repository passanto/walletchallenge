package bet.pixel.wallet.repositories;

import bet.pixel.wallet.models.Transaction;
import org.springframework.data.repository.CrudRepository;

import java.math.BigDecimal;
import java.util.Optional;

public interface TransactionRepository extends CrudRepository<Transaction, Long> {

    Optional<Transaction> findTransactionByAccount_PlayerIdAndExternalTransactionIdAndAmount(
            long playerId, String externalTransactionId, BigDecimal amount);

}
