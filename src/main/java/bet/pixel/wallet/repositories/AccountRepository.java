package bet.pixel.wallet.repositories;

import bet.pixel.wallet.models.Account;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.repository.CrudRepository;

import javax.persistence.LockModeType;
import java.util.Optional;

public interface AccountRepository extends CrudRepository<Account, Long> {

    @Lock(LockModeType.OPTIMISTIC)
    Optional<Account> findAccountByPlayerId(long playerId);

}
