package bet.pixel.wallet.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class TransactionRequestException extends Exception {

    public TransactionRequestException(String message) {
        super(message);
    }
}
