package bet.pixel.wallet.controllers;

import bet.pixel.wallet.exception.NotFoundException;
import bet.pixel.wallet.exception.TransactionRequestException;
import bet.pixel.wallet.models.Account;
import bet.pixel.wallet.models.Transaction;
import bet.pixel.wallet.services.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.List;

@RestController
@RequestMapping("/wallet")
public class WalletController {

    private WalletService walletService;

    @GetMapping("/account")
    public Account getAccount(@RequestParam(name = "player-id") long playerId) {
        return walletService.getAccount(playerId);
    }

    @PostMapping("/account")
    public Account addAccount(@RequestParam(name = "player-id") long playerId) {
        return walletService.addAccount(playerId);
    }

    @GetMapping("/balance")
    public BigDecimal getBalanceByPlayerId(@RequestParam(name = "player-id") long playerId) throws NotFoundException {
        return walletService.getBalanceByPlayerId(playerId);
    }

    @GetMapping("/transactions")
    public List<Transaction> getTransactionsByPlayerId(@RequestParam(name = "player-id") long playerId) throws NotFoundException {
        return walletService.getTransactionsByPlayerId(playerId);
    }

    @PostMapping("/debit")
    public BigDecimal debitForPlayer(@RequestParam(name = "player-id") long playerId,
                                     @RequestParam(name = "transaction-id") String transactionId,
                                     @RequestParam(name = "external-transaction-id") String externalTransactionId,
                                     @RequestParam BigDecimal amount) throws TransactionRequestException, NotFoundException {
        return walletService.debitForPlayer(playerId, transactionId, amount, externalTransactionId);
    }

    @PostMapping("/credit")
    public BigDecimal creditForPlayer(@RequestParam(name = "player-id") long playerId,
                                      @RequestParam(name = "transaction-id") String transactionId,
                                      @RequestParam(name = "external-transaction-id") String externalTransactionId,
                                      @RequestParam BigDecimal amount) throws TransactionRequestException, NotFoundException {
        return walletService.creditForPlayer(playerId, transactionId, amount, externalTransactionId);
    }

    @Autowired
    public void setWalletService(WalletService walletService) {
        this.walletService = walletService;
    }

}
