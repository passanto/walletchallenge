package bet.pixel.wallet.services;

import bet.pixel.wallet.Constants;
import bet.pixel.wallet.exception.NotFoundException;
import bet.pixel.wallet.exception.TransactionRequestException;
import bet.pixel.wallet.models.Account;
import bet.pixel.wallet.models.Transaction;
import bet.pixel.wallet.repositories.AccountRepository;
import bet.pixel.wallet.repositories.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import static java.lang.String.format;

@Service
public class WalletService {

    private AccountRepository accountRepository;
    private TransactionRepository transactionRepository;

    public Account getAccount(long playerId) {
        return accountRepository.findById(playerId).orElseThrow(
                () -> new DataIntegrityViolationException(format("No account for player %s found", playerId))
        );
    }

    public Account addAccount(long playerId) {
        accountRepository.findById(playerId)
                .ifPresent(a -> {
                            throw new DataIntegrityViolationException(format("Account for player %s already exists", playerId));
                        }
                );
        return accountRepository.save(new Account(playerId));
    }

    @Transactional
    public BigDecimal getBalanceByPlayerId(long playerId) throws NotFoundException {
        var account = accountRepository.findAccountByPlayerId(playerId)
                .orElseThrow(() -> new NotFoundException(format(Constants.NO_ACCOUNT_FOR_PLAYER_ID_S, playerId)));
        return account.getBalance();
    }

    @Transactional
    public List<Transaction> getTransactionsByPlayerId(long playerId) throws NotFoundException {
        var account = accountRepository.findAccountByPlayerId(playerId)
                .orElseThrow(() -> new NotFoundException(format(Constants.NO_ACCOUNT_FOR_PLAYER_ID_S, playerId)));
        return account.getTransactions();
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    public BigDecimal debitForPlayer(long playerId, String transactionId, BigDecimal amount, String externalTransactionId)
            throws TransactionRequestException, NotFoundException {
        // check for idempotent transaction
        Optional<Transaction> optionalTransaction = transactionRepository.
                findTransactionByAccount_PlayerIdAndExternalTransactionIdAndAmount(playerId, externalTransactionId, amount.negate());
        if (optionalTransaction.isPresent()) {
            // if found an already processed transaction, its balance is returned
            return optionalTransaction.get().getBalance();
        }

        // validation
        var account = validateTransactionRequest(playerId, transactionId);

        BigDecimal balance = account.getBalance();
        if (balance.subtract(amount).compareTo(BigDecimal.ZERO) < 0) {
            throw new TransactionRequestException(format(Constants.NOT_ENOUGH_FUNDS, playerId));
        }
        BigDecimal newBalance = account.getBalance().subtract(amount);
        var newTransaction = new Transaction(account, amount.negate(), newBalance, externalTransactionId);
        transactionRepository.save(newTransaction);

        account.getTransactions().add(newTransaction);
        account.setBalance(newBalance);
        accountRepository.save(account);
        return account.getBalance();
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    public BigDecimal creditForPlayer(long playerId, String transactionId, BigDecimal amount, String externalTransactionId)
            throws TransactionRequestException, NotFoundException {
        // check for idempotent transaction
        Optional<Transaction> optionalTransaction = transactionRepository.
                findTransactionByAccount_PlayerIdAndExternalTransactionIdAndAmount(playerId, externalTransactionId, amount);
        if (optionalTransaction.isPresent()) {
            // if found an already processed transaction, its balance is returned
            return optionalTransaction.get().getBalance();
        }

        // validation
        var account = validateTransactionRequest(playerId, transactionId);

        BigDecimal newBalance = account.getBalance().add(amount);
        var newTransaction = new Transaction(account, amount, newBalance, externalTransactionId);
        transactionRepository.save(newTransaction);

        account.getTransactions().add(newTransaction);
        account.setBalance(newBalance);
        accountRepository.save(account);
        return account.getBalance();
    }

    private Account validateTransactionRequest(long playerId, String transactionId)
            throws TransactionRequestException, NotFoundException {
        var account = accountRepository.findAccountByPlayerId(playerId)
                .orElseThrow(() -> new NotFoundException(format(Constants.NO_ACCOUNT_FOR_PLAYER_ID_S, playerId)));
        if (account.getTransactionId() != null &&
                !account.getTransactionId().equals(transactionId)) {
            throw new TransactionRequestException(Constants.INVALID_TX_ID);
        }
        if (account.getTransactionId() == null) {
            account.setTransactionId(transactionId);
            return accountRepository.save(account);
        }
        return account;
    }

    @Autowired
    public void setAccountRepository(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Autowired
    public void setTransactionRepository(TransactionRepository transactionRepository) {
        this.transactionRepository = transactionRepository;
    }
}
