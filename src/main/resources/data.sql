CREATE TABLE IF NOT EXISTS account
(
    account_id     BIGINT AUTO_INCREMENT PRIMARY KEY,
    player_id      BIGINT  NOT NULL,
    transaction_id VARCHAR,
    version        INT,
    balance        DECIMAL NOT NULL DEFAULT 0
);

CREATE TABLE IF NOT EXISTS transaction
(
    id                      BIGINT AUTO_INCREMENT PRIMARY KEY,
    account_id              BIGINT  NOT NULL,
    amount                  DECIMAL NOT NULL,
    balance                 DECIMAL NOT NULL DEFAULT 0,
    external_transaction_id VARCHAR NOT NULL,
    FOREIGN KEY (account_id) REFERENCES account (account_id)
);

CREATE SEQUENCE IF NOT EXISTS hibernate_sequence START WITH 1 INCREMENT BY 1;

