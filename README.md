# Pixel.bet Wallet Challenge

### Prerequisites  
* Java 11  
* Maven 3  

### Test the project
```$ mvn test```

### Run the project
```$ mvn spring-boot:run```

### H2 in memory database
The console is available at the following addres:  
http://localhost:8080/pixelbet/h2-console  
username: sa  
password:

<b>Embedded mode</b>:  
A <i>walletDB</i> file is being created in the <i>target</i> folder.  
All changes are persisted across restarts.  


### Test the project
The IntelliJ compatible <i>wallet-requests.http</i> file, located at the root of the project, can be used to send REST calls to the api


### Assumptions
#### Persistence

#### Models
The submitter assumes that a player can own more than one account, as such no uniqueness constraint has been applied to the model in this perspective.
#### Unique Transaction Id
In order to fulfill the uniqueness criteria for the <b>supplied transaction id</b>, the submitter assumes that this parameter is being provided by the consumer in order to achieve some sort of security.  
As such, this identifier is statically associated with an account, which will be not valued on a newly created account.  
Once the first transaction is received for such account, it gets persisted as the identifier with which all the next transactions will be verified against.  

The submitter also assumes that in a real scenario, this identifier can change based on some specific consumer criteria:  
* session based
* time based
* other parameter values (game id, campaign id...)  

each of the above can be managed in specific manners.
<br/><br/>
#### Idempotency
In order to provide idempotent debit/credit calls, the submitter has introduced an additional parameter that the consumer has to provide for such calls.  
This <b>externalTransactionId</b> is being used in conjunction with the <b>playerId</b> and the <b>amount</b> in subject, in order to retrieve the eventual already persisted transaction.
If such a transaction exists, its balance is returned in order to achieve idempotency.  
The benefit to store the <i>updated balance</i> for each and every transaction is also an additional level of auditing and traceability in order to better investigate transactions history's consistence.  
</br>
The transactional service methods (debit and credit) are annotated with a transactional isolation level of <b>READ_COMMITTED</b>.  
With this approach, only committed <i>transaction</i> entities can be read, this way the application guarantees idempotency on each and every call.
</br>
</br>
Note: checked exception are thrown before any save/update statement, as such the rollback can be the default approach (only on unchecked exception).
### Concurrency
For <b>concurrency</b> purposes, an Optimistic Lock Type is used on the account entity, specifically on the select query since avoiding dirty reads is mandatory for the transaction id integrity check.  
</br>
Say thread_A is trying to "initialise" the <b>transactionId</b> field of an account which was null at time of reading.  
If thread_B manages to update the same account (for whatever reason), thread_A fails in persisting the account entity.  

This approach could of course lead to some calls to fail, however being the API built for idempotency, the client can eventually re-send a failed transaction and get a reliable result.    
### Scalability
Being the application completely stateless, it can be scaled configuring a Kubernetes cluster to create as many pods as required.  
If a protection mechanism is required, Spring Security with JWT token authentication can be evaluated being a standard solution.

